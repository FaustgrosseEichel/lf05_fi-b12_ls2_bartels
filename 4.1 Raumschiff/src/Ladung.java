
public class Ladung {

	private String bezeichnung;
	private int  menge;
	private String bezeichnung2;
	private int menge2;
	
	
	public Ladung(String bezeichnung, int menge, String bezeichnung2, int menge2) {
		super();
		this.bezeichnung = bezeichnung;
		this.menge = menge;
		this.bezeichnung2 = bezeichnung2;
		this.menge2 = menge2;
	}

	public Ladung(String bezeichnung, int menge) {
		super();
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	@Override
	public String toString() {
		return "Ladung \n\nbezeichnung=" + bezeichnung + ", \nmenge=" + menge + " ";
	}

	/**
	 * @return the bezeichnung2
	 */
	public String getBezeichnung2() {
		return bezeichnung2;
	}

	/**
	 * @param bezeichnung2 the bezeichnung2 to set
	 */
	public void setBezeichnung2(String bezeichnung2) {
		this.bezeichnung2 = bezeichnung2;
	}

	/**
	 * @return the menge2
	 */
	public int getMenge2() {
		return menge2;
	}

	/**
	 * @param menge2 the menge2 to set
	 */
	public void setMenge2(int menge2) {
		this.menge2 = menge2;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	public int getMenge() {
		return menge;
	}
	public void setMenge(int menge) {
		this.menge = menge;
	}

	}



