public class RaumschiffTest {

	public static void main(String[] args)
	{
	Raumschiff Klingonen = new Raumschiff(1,100,100,100,100,2,"IKS Hegh'ta");
    Raumschiff Romulaner = new Raumschiff(2,100,100,100,100,2,"IRW Khazara");
    Raumschiff Vulkanier = new Raumschiff(0,80,80,50,100,5,"Ni'Var");
	
	System.out.println("Raumschiff 1 = " + Klingonen);
	System.out.println("\nRaumschiff 2 = " + Romulaner);
	System.out.println("\nRaumschiff 3 = " + Vulkanier);

    Ladung l1 = new Ladung("Ferengi Schneckensaft",200);
    Ladung l2 = new Ladung("Borg-Schrott", 5);
    Ladung l3 = new Ladung("Forschungssonde", 35);
    
    System.out.println (l1);
    System.out.println (l2);
    System.out.println (l3);
	}
}
